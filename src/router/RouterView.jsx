import React, { Component, Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

export class RouterView extends Component {
  render() {
    let { routes = [] } = this.props;
    let routeArr = routes.filter((item) => !item.redirect && item.path !== '*');
    let redirectArr = routes
      .filter((item) => item.redirect)
      .map((item, index) => <Redirect exact key={index} from={item.path} to={item.redirect} />);
    let routesNF = routes.filter((item) => item.path === '*');
    return (
      <Suspense fallback={<div>loading...</div>}>
        <Switch>
          {routeArr &&
            routeArr
              .map((item, index) => {
                return (
                  <Route
                    key={index}
                    path={item.path}
                    render={(props) => {
                      return <item.component {...props} routes={item.children} />;
                    }}
                  />
                );
              })
              .concat(redirectArr)}
          {routesNF &&
            routesNF.map((item, index) => {
              return (
                <Route
                  key={index}
                  path={item.path}
                  render={(props) => {
                    return <item.component {...props} />;
                  }}
                />
              );
            })}
        </Switch>
      </Suspense>
    );
  }
}

export default RouterView;
