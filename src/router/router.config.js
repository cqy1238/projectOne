import { lazy } from 'react';
export const homeRouters = [
  {
    path: '/home/article',
    meta: {
      title: 'article',
    },
    component: lazy(() => import('../views/article/article')),
  },
  {
    path: '/home/thearchive',
    meta: {
      title: 'category',
    },
    component: lazy(() => import('../views/TheArchive/Archive')),
  },
  {
    path: '/home/knowledge',
    meta: {
      title: 'knowledge',
    },
    component: lazy(() => import('../views/KnowledgeBooklet/KnowledgeBooklet')),
  },
  {
    path: '/home/board',
    meta: {
      title: 'message',
    },
    component: lazy(() => import('../views/board/board.jsx')),
  },
  {
    path: '/home/about',
    meta: {
      title: 'about',
    },
    component: lazy(() => import('../views/about/About')),
  },
];
const routes = [
  {
    path: '/home',
    component: lazy(() => import('../views/Home')),
    children: [
      ...homeRouters,
      {
        path: '/home/Knowledgedetailed',
        component: lazy(() => import('../views/KnowledgeBooklet/Knowledgedetailed')),
      },
      {
        path: '*',
        component: lazy(() => import('../views/error/Error.jsx')),
      },
      {
        path: '/home',
        redirect: '/home/article',
      },
    ],
  },
  //列表详情
  {
    path: '/detall',
    component: lazy(() => import('../views/detallPage/detall')),
  },
  //banner详情
  {
    path: '/bannerdetail',
    component: lazy(() => import('../views/bannerfetail/bannerdetall.jsx')),
  },
  //知识小测详情
  {
    path: '/knowdetall',
    component: lazy(() => import('../views/knowledgeBookietdetall/knowdetall.jsx')),
  },
  {
    path: '/category/:value',
    component: lazy(() => import('../views/article/category')),
  },
  {
    path: '/tag/:value',
    component: lazy(() => import('../views/article/Tag')),
  },
  {
    path: '*',
    component: lazy(() => import('../views/error/Error.jsx')),
  },
  {
    path: '/',
    redirect: '/home',
  },
];

export default routes;
