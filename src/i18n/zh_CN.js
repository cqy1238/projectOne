const data = {
  myMessage: '哈喽',
  article: '文章',
  category: '归档',
  knowledge: '知识小册',
  message: '留言板',
  about: '关于',
  english: '英文',
  chinese: '汉语',
};

export default data;
