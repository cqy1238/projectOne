const data = {
  myMessage: 'hello',
  article: 'Article',
  category: 'Category',
  knowledge: 'Knowledge',
  message: 'Message',
  about: 'About',
  english: 'English',
  chinese: 'Chinese',
};

export default data;
