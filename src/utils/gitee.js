const giteeConfig = {
  client_id: 'dc2324df06eee781692ecde9926360efc1d4cec4830bdf5a53332f9297cdd2c0',
  redirect_uri: 'http://localhost:3000/home',
};

// 跳转第三方登陆页面
export const toGitee = () => {
  window.location.href = `https://gitee.com/oauth/authorize?client_id=${giteeConfig.client_id}&redirect_uri=${giteeConfig.redirect_uri}&response_type=code`;
};
// 获取验证码
export const getCode = () => {
  const code = window.location.search.slice(1).split('=')[1];
  return code;
};
