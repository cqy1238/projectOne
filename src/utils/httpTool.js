import axios from 'axios';
import httpCode from './httpCode';
const httpTool = ({
  timeout = 10,
  commontHeaders = {},
  failMessage = (msg) => {
    alert(msg);
  },
}) => {
  const httpAxios = axios.create({
    timeout: timeout * 1000,
  });
  // 添加请求拦截器
  httpAxios.interceptors.request.use(
    function (config) {
      // 在发送请求之前做些什么
      return {
        ...config,
        headers: {
          ...config.headers,
          ...commontHeaders,
        },
      };
    },
    function (error) {
      // 对请求错误做些什么
      return Promise.reject(error);
    }
  );
  // 添加响应拦截器
  httpAxios.interceptors.response.use(
    function (response) {
      // 对响应数据做点什么
      if (response.data.statusCode === 200) {
        return response;
      }
      failMessage(
        response.data.msg || httpCode[response.data.statusCode] || '服务器出现未知错误，请重新请求'
      );
    },
    function (error) {
      // 对响应错误做点什么
      //   const status = error.response.status;
      if (error.code === 'ECONNABORTED') {
        failMessage('请求超时');
      }
      return Promise.reject(error);
    }
  );
  return httpAxios;
};

export default httpTool;
