import axios from 'axios';
const request = axios.create({
  timeoue: 2000,
});
// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    const status = error.response.status;
    if (status === 404) {
      // eslint-disable-next-line no-alert
      alert('接口找不到');
    } else if (status === 500) {
      // eslint-disable-next-line no-alert
      alert('服务端错误');
    } else if (status === 304) {
      // eslint-disable-next-line no-alert
      alert('信息被缓存了');
    } else if (status === 400) {
      // eslint-disable-next-line no-alert
      alert('语义有误或者请求参数错误');
    } else if (status === 423) {
      // eslint-disable-next-line no-alert
      alert('正在访问的资源被锁定');
    }
    return Promise.reject(error);
  }
);

export default request;
