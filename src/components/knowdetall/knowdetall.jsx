import React, { Component } from 'react';
import { Anchor, Image } from 'antd';
import { detallList, detaLIS } from '../../api/detall';
import './deta.css';
import './less/markdown.less';
import { IndexBar } from 'react-vant';
import { Link } from 'react-router-dom';
// console.log(Link);

const indexList = [];

const customIndexList = [1, 2, 3, 4, 5, 6, 8, 9, 10];
const charCodeOfA = 'A'.charCodeAt(0);

for (let i = 0; i < 26; i += 1) {
  indexList.push(String.fromCharCode(charCodeOfA + i));
}

// const { Link } = Anchor;
export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
      ars: [],
      visible: false,
    };
  }
  componentDidMount() {
    // detallList().then((res) => {
    //   this.setState({
    //     arr: res.data.data,
    //   });
    // });
    detaLIS().then((res) => {
      this.setState({
        ars: res.data.data,
      });
      // console.log(res.data.data,'打印的结果');
    });
  }
  setVisible() {
    console.log(11);
    this.setState({
      visible: true,
    });
  }
  tiao() {
    console.log(11);
    // this.props
  }

  render() {
    const { ars } = this.state;
    // console.log(this.props);
    const { data, arsList } = this.props;
    // console.log(arsList, "**********************************");
    let { visible } = this.state;
    let str = [];
    // str.push(arr.toc);

    return (
      <div className="detallPage">
        <div className="detall_left">
          <div className="detallabout">
            <div className="about">
              <div className="about_in">
                {data && (
                  <Image
                    preview={false}
                    width={200}
                    src={data.cover}
                    onClick={this.setVisible.bind(this)}
                  />
                )}
                <div
                  style={{
                    display: 'none',
                  }}
                >
                  <Image.PreviewGroup
                    preview={{
                      visible,
                      onVisibleChange: () => {
                        this.setState({
                          visible: false,
                        });
                      },
                    }}
                  >
                    {data && <Image src={data.cover} />}
                    {data && <Image src={data.cover} />}
                  </Image.PreviewGroup>
                </div>
              </div>
            </div>
          </div>
          <div className="detatitle">{data && <h1>{data.title}</h1>}</div>
          {/* {arr&&arr.content} */}

          <div className="markdown" dangerouslySetInnerHTML={{ __html: data.html }}></div>

          <div className="detallbott">
            <h2>全部数据以展示</h2>
          </div>

          <div></div>

          <div className="bottom">
            <h2>推荐阅读</h2>
            {arsList[0] &&
              arsList[0].map((v, i) => {
                return (
                  <Link
                    className="arslis"
                    to={{
                      pathname: '/detall',
                      state: v.id,
                    }}
                    key={v.id}
                  >
                    <h2>{v.title}</h2>
                    <img src={v.cover} alt="加载失败"></img>
                  </Link>
                );
              })}
          </div>
        </div>
        <div className="detall_right">
          {/* <div className="detall_top">
            <h1>推荐阅读</h1>
            {ars.length
              ? ars.map((v, i) => {
                  return <h2 key={i}>{v.title}</h2>;
                })
              : '暂无数据'}
          </div> */}
          <div>
            <IndexBar indexList={customIndexList}>
              {customIndexList.map((item) => (
                <div key={item}>
                  <IndexBar.Anchor index={item} className="lcc"></IndexBar.Anchor>
                </div>
              ))}
            </IndexBar>
          </div>
        </div>
        <div></div>
      </div>
    );
  }
}
