import React, { useState, useCallback, useEffect } from 'react';
import { thearchiveList } from '../../api/thearchive';
import './RightRead.css';
import { Link } from 'react-router-dom';
export default function RightRead() {
  const [list, setlist] = useState([]);
  const [year, setyear] = useState('');
  const [month, setmonth] = useState('');
  let init = useCallback(async () => {
    const res = await thearchiveList();
    const years = Object.keys(res.data.data)[0];
    setyear(years);

    const month = Object.keys(res.data.data[years])[0];
    setmonth(month);

    const listData = res.data.data[years][month];
    setlist(listData);
  }, []);

  useEffect(() => {
    init();
  }, [init]);
  let ctime = list.map((item, index) => {
    let ztime = Date.now() - new Date(item.createAt).getTime();
    //秒
    let second = parseInt((ztime / 1000) % 60);
    //分钟
    let minute = parseInt((ztime / 1000 / 60) % 60);
    //小时
    let hour = parseInt((ztime / 1000 / 60 / 60) % 24);
    //天数
    let day = parseInt((ztime / 1000 / 60 / 60 / 24) % 30);
    //月
    let mounth = parseInt((ztime / 1000 / 60 / 60 / 24 / 30) % 12);
    //年
    let year = parseInt(ztime / 1000 / 60 / 60 / 24 / 30 / 12);
    // console.log(`${year}年${mounth}月${day}天${hour}小时${minute}分${second}秒`);
    let ctime =
      year > 0
        ? year + '年'
        : mounth > 0
        ? mounth + '月'
        : day > 0
        ? day + '天'
        : hour > 0
        ? hour + '小时'
        : minute > 0
        ? minute + '分钟'
        : second + '秒';
    return ctime;
  });
  return (
    <div className="rightread">
      <h3>推荐阅读</h3>
      <div className="rightread_list">
        <ul>
          {list && list.length > 0 ? (
            list.map((item, index) => {
              return (
                <Link key={index} to={`/detall/${item.id}`}>
                  <li>
                    <span>{item.title}</span>
                    <span className="point"></span>
                    <span>大约{ctime[index]}前</span>
                  </li>
                </Link>
              );
            })
          ) : (
            <div>暂无数据</div>
          )}
        </ul>
      </div>
    </div>
  );
}
