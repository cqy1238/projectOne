import React, { Component } from 'react';
import './comment.css';
import axios from '../../utils/axios';
import { Avatar, Button, Comment, Form, Input, List } from 'antd';
import { Pagination } from 'antd';
import { inject, observer } from 'mobx-react';
import LoginModal from '../header/loginModal/loginModal';
import { MessageOutlined, SmileOutlined } from '@ant-design/icons';
import { message } from 'antd';

export class Comments extends Component {
  state = {
    pageNum: 1,
    pageSize: 6,
    loginFlag: false,
    emojiFlag: false,
  };
  componentDidMount() {
    let { pageNum, pageSize } = this.state;
    this.props.commentStore.getList({ pageNum, pageSize });
  }
  render() {
    let loginState = localStorage.getItem('isLogin');
    let rvalue = '';
    // 评论输入框
    const Editor = ({ onChange, onSubmit, submitting, value }) => (
      <>
        <Form.Item>
          <Input
            rows={4}
            onChange={(e) => {
              let val = e.target.value;
              value = val;
            }}
            value={value}
            id="comment_area"
            placeholder="请输入评论内容（支持 Markdown）"
          />
        </Form.Item>
        {/* 表情logo */}
        <Form.Item>
          <p
            className="emojiFont"
            onClick={() => {
              this.setState({
                emojiFlag: !this.state.emojiFlag,
              });
            }}
          >
            <SmileOutlined />
            表情
          </p>
          {/* 发布按钮 */}
          <Button
            htmlType="submit"
            loading={submitting}
            onClick={() => {
              // 判断登录态
              if (loginState) {
                this.setState({
                  loginFlag: false,
                });
                // 如果已登录，判断输入框内有没有内容
                if (value) {
                  this.props.commentStore.getList({ pageNum, pageSize });
                  axios
                    .post('/api/comment', {
                      content: value,
                      email: value,
                      name: JSON.parse(localStorage.getItem('isLogin')).username,
                      hostId: '10257f2f-3367-4183-b496-27baf0f3743e',
                      url: '/article/10257f2f-3367-4183-b496-27baf0f3743e',
                    })
                    .then((res) => {
                      console.log(res);
                      message.success('发布成功');
                    });
                } else {
                  message.warning('请输入内容');
                }
              } else {
                this.setState({
                  loginFlag: true,
                });
              }
            }}
            type="primary"
            id="comment_btn"
          >
            发布
          </Button>
        </Form.Item>
      </>
    );
    // 所有表情的数据
    const emojiArr = [
      '😀',
      '😃',
      '😄',
      '😁',
      '😆',
      '😆',
      '😅',
      '😂',
      '😉',
      '😊',
      '😇',
      '😍',
      '😘',
      '😗',
      '😚',
      '😙',
      '😋',
      '😛',
      '😜',
      '😝',
      '😐',
      '😑',
      '😶',
      '😏',
      '😒',
      '😌',
      '😔',
      '😪',
      '😴',
      '😷',
      '😵',
      '😎',
      '😕',
      '😟',
      '😮',
      '😯',
      '😲',
      '😳',
      '😦',
      '😧',
      '😨',
      '😰',
      '😥',
      '😢',
      '😭',
      '😱',
      '😖',
      '😣',
      '😞',
      '😓',
      '😩',
      '😫',
      '😡',
      '😡',
      '😠',
      '😈',
      '😺',
      '😸',
      '😹',
      '😻',
      '😼',
      '😽',
      '🙀',
      '😿',
      '😾',
      '❤️',
      '✋',
      '✋',
      '✌️',
      '☝️',
      '✊',
      '✊',
      '🐵',
      '🐱',
      '🐮',
      '🐭',
      '☕',
      '♨️',
      '⚓',
      '✈️',
      '⌛',
      '⌚',
      '☀️',
      '⭐',
      '☁️',
      '☔',
      '⚡',
      '❄️',
      '✨',
      '🃏',
      '🀄',
      '☎️',
      '☎️',
      '✉️',
      '✏️',
      '✒️',
      '✂️',
      '♿',
      '⚠️',
      '♈',
      '♉',
      '♊',
      '♋',
      '♌',
      '♍',
      '♎',
      '♏',
      '♐',
      '♑',
      '♒',
      '♓',
      '✖️',
      '➕',
      '➖',
      '➗',
      '‼️',
      '⁉️',
      '❓',
      '❔',
      '❕',
      '❗',
      '❗',
      '〰️',
      '♻️',
      '✅',
      '☑️',
      '✔️',
      '❌',
      '❎',
      '➰',
      '➿',
      '〽️',
      '✳️',
      '✴️',
      '❇️',
      '©️',
      '®️',
      '™️',
      'ℹ️',
      'Ⓜ️',
      '⚫',
      '⚪',
      '⬛',
      '⬜',
      '◼️',
      '◻️',
      '◾',
      '◽',
      '▪️',
      '▫️',
    ];
    // 拿到mobx的数据
    let { list, length, pageSize, pageNum } = this.props.commentStore;
    return (
      <div className="comments">
        <div className="comments_in">
          {/* 登录框 */}
          <LoginModal
            loginModalVisible={this.state.loginFlag}
            setLoginModalVisible={() => {
              this.setState({
                loginFlag: false,
              });
            }}
          />
          <p className="title">评论</p>
          <div className="list">
            {/* 评论区输入框 */}
            <Comment
              avatar={
                <Avatar
                  src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi-1.lanrentuku.com%2F2020%2F11%2F5%2Fdef6ed04-6d34-402e-99c8-366266f627dd.png%3FimageView2%2F2%2Fw%2F500&refer=http%3A%2F%2Fi-1.lanrentuku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1656082194&t=a83a23a1cc9f7477ddb6e6273bc676cf"
                  alt="Han Solo"
                />
              }
              content={<Editor />}
            />
            {/* 表情栏 */}
            <ul id="emojiBox" style={{ display: this.state.emojiFlag ? 'block' : 'none' }}>
              {emojiArr &&
                emojiArr.map((item, index) => {
                  return (
                    <li
                      key={index}
                      onClick={(e) => {
                        rvalue += e.target.innerHTML;
                      }}
                    >
                      {item}
                    </li>
                  );
                })}
            </ul>
            {/* 所有评论列表 */}
            {list &&
              list.map((item, index) => {
                return (
                  <dl key={index} className="comment_list">
                    <dd>
                      <img
                        src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi-1.lanrentuku.com%2F2020%2F11%2F5%2Fdef6ed04-6d34-402e-99c8-366266f627dd.png%3FimageView2%2F2%2Fw%2F500&refer=http%3A%2F%2Fi-1.lanrentuku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1656082194&t=a83a23a1cc9f7477ddb6e6273bc676cf"
                        alt=""
                      />
                    </dd>
                    <dd>
                      <p>{item.name}</p>
                      <p>{item.content}</p>
                      <span className="userAgent">{item.userAgent}</span>
                      <span
                        className="reply"
                        onClick={() => {
                          if (loginState) {
                            this.setState({
                              loginFlag: false,
                            });
                          } else {
                            this.setState({
                              loginFlag: true,
                            });
                          }
                        }}
                      >
                        <MessageOutlined />
                        回复
                      </span>
                    </dd>
                  </dl>
                );
              })}
          </div>
        </div>
        {/* 分页模块 */}
        <div className="page_box">
          <Pagination
            size="small"
            // 总条数
            total={length}
            // 每页显示多少条
            defaultPageSize={pageSize}
            // 当前页码
            defaultCurrent={pageNum}
            showSizeChanger={false}
            onChange={(e) => {
              this.props.commentStore.setPageNum(e);
              let top = document.documentElement.scrollTop || document.body.scrollTop;
              // 实现滚动效果
              const timeTop = setInterval(() => {
                document.body.scrollTop = document.documentElement.scrollTop = top -= 5;
                if (top <= 233) {
                  clearInterval(timeTop);
                }
              }, 1);
            }}
          />
        </div>
      </div>
    );
  }
}

export default inject('commentStore')(observer(Comments));
