import React, { Component } from 'react';
import { Image } from 'antd';
import { detaLIS } from '../../api/detall';
import './deta.css';
import './less/markdown.less';
import { Link } from 'react-router-dom';
import Comments from '../Comments/Comments';

// const { Link } = Anchor;
export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
      ars: [],
      visible: false,
      ind: 0,
    };
  }
  HandleScroll = (e) => {
    const h1 = document.querySelectorAll('.detallPage h1');
    const h2 = document.querySelectorAll('.detallPage h2');
    const h3 = document.querySelectorAll('.detallPage h3');
    if (h1[1]) {
      if (document.documentElement.scrollTop > h1[1].offsetTop) {
        this.setState({ ind: 0 });
      }
    }
    h2.forEach((item, index) => {
      if (document.documentElement.scrollTop > item.offsetTop) {
        this.setState({ ind: index + 1 });
      }
    });
    h3.forEach((item, index) => {
      if (document.documentElement.scrollTop > item.offsetTop) {
        this.setState({ ind: index + 2 });
      }
    });
  };
  toScroll = (item, index) => {
    document.getElementById(item.id)?.scrollIntoView();
    this.setState({ ind: index });
  };
  componentDidMount() {
    window.addEventListener('scroll', this.HandleScroll);
    detaLIS().then((res) => {
      this.setState({
        ars: res.data.data,
      });
      // console.log(res.data.data,'打印的结果');
    });
  }
  setVisible() {
    // console.log(11);
    this.setState({
      visible: true,
    });
  }
  tiao() {
    // console.log(11);
    // this.props
  }

  render() {
    // const { ars } = this.state;
    // console.log(this.props);
    const { data, arsList } = this.props;
    // console.log(arsList, '**********************************');
    let { visible } = this.state;
    // let str = [];
    // str.push(arr.toc);

    return (
      <div className="detallPage">
        <div className="detall_left">
          <div className="detallabout">
            <div className="about">
              <div className="about_in">
                {data && (
                  <Image
                    preview={false}
                    width={200}
                    src={data.cover}
                    onClick={this.setVisible.bind(this)}
                  />
                )}
                <div
                  style={{
                    display: 'none',
                  }}
                >
                  <Image.PreviewGroup
                    preview={{
                      visible,
                      onVisibleChange: () => {
                        this.setState({
                          visible: false,
                        });
                      },
                    }}
                  >
                    {data && <Image src={data.cover} />}
                    {data && <Image src={data.cover} />}
                  </Image.PreviewGroup>
                </div>
              </div>
            </div>
          </div>
          <div className="detatitle">{data && <h1>{data.title}</h1>}</div>
          {/* {arr&&arr.content} */}

          <div className="markdown" dangerouslySetInnerHTML={{ __html: data.html }}></div>
          {/* 
          <div className="detallbott">
            <h2>全部数据以展示</h2>
          </div> */}

          <div className="commect">
            <Comments></Comments>
          </div>

          <div className="detall_bottom">
            <h2>推荐阅读</h2>
            {arsList &&
              arsList.map((v, i) => {
                return (
                  <Link
                    className="arslis"
                    to={{
                      pathname: '/detall',
                      state: v.id,
                    }}
                    key={i}
                  >
                    <h2>{v.title}</h2>
                    <img src={v.cover} alt="加载失败"></img>
                  </Link>
                );
              })}
          </div>
        </div>
        <div className="detall_rightss">
          {/* <div className="detall_top">
            <h1>推荐阅读</h1>
            {ars.length
              ? ars.map((v, i) => {
                  return <h2 key={i}>{v.title}</h2>;
                })
              : '暂无数据'}
          </div> */}
          <div>
            {/* <IndexBar indexList={customIndexList}>
              {customIndexList.map((item) => (
                <div key={item}>
                  <IndexBar.Anchor index={item} className="lcc"></IndexBar.Anchor>
                </div>
              ))}
            </IndexBar> */}
          </div>
        </div>
        <div className="bannerulli">
          {
            <ul>
              {data.toc
                ? JSON.parse(data.toc).map((item, index) => {
                    return (
                      <div key={item.id} className={this.state.ind === index ? `ons` : ''}>
                        <li
                          onClick={() => {
                            this.toScroll(item, index);
                          }}
                          className={`li_${item.level}`}
                        >
                          {item.text}
                        </li>
                      </div>
                    );
                  })
                : ''}
            </ul>
          }
        </div>
      </div>
    );
  }
}
