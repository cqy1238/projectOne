import React, { Component } from 'react';
import axios from '../../utils/axios';
import ArtItem from '../../components/Item/item';
import './bottom_read.css';
import { NavLink } from 'react-router-dom';
import { getRecRead } from '../../api/bottomrecread';
import Share from '../Share';
export class BottomRead extends Component {
  state = {
    data: [],
    vis: false,
    record: {},
  };
  componentDidMount() {
    getRecRead().then((res) => {
      this.setState({
        data: res.data.data,
      });
    });
  }
  render() {
    let { data, vis, record } = this.state;
    return (
      <div className="BottomRead">
        <p className="BR_title">推荐阅读</p>
        <div className="data">
          <Share
            vis={vis}
            item={record}
            hideModel={() => {
              this.setState({ vis: false });
            }}
          />
          {data &&
            data.map((item) => {
              return (
                <NavLink to={{ pathname: '/detall', state: item.id }} key={item.id}>
                  <ArtItem
                    item={item}
                    showModel={() => {
                      this.setState({
                        vis: !this.state.vis,
                        record: item,
                      });
                    }}
                  />
                </NavLink>
              );
            })}
        </div>
      </div>
    );
  }
}

export default BottomRead;
