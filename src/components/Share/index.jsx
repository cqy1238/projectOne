import React, { useState, useEffect } from 'react';
import { Modal, Image } from 'antd';
import QRCode from 'qrcode.react';
const Share = ({ vis, hideModel, item }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  useEffect(() => {
    setIsModalVisible(vis);
  }, [vis]);
  const handleOk = (e) => {
    e.stopPropagation();
    hideModel(false);
  };

  const handleCancel = (e) => {
    e.stopPropagation();
    hideModel(false);
  };
  return (
    <Modal
      title="分享海报"
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      cancelText={'关闭'}
      okText={'确认'}
    >
      <Image src={item.cover} preview={false} width={'100%'} />
      <p style={{ fontSize: '18px', fontWeight: '800' }}>{item.title}</p>
      <p>{item.summary}</p>
      <dl style={{ display: 'flex' }}>
        <dt>
          <QRCode value={window.href + item.id} renderAs="canvas" />
        </dt>
        <dd
          style={{
            marginLeft: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <p>识别二维码查看文章</p>
          <p>
            原文分享自<a href="https://blog.wipi.tech/">小楼又清风</a>
          </p>
        </dd>
      </dl>
    </Modal>
  );
};

export default Share;
