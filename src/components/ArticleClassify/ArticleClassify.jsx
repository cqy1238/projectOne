import React, { useState, useCallback, useEffect } from 'react';
import './ArticleClassify.css';
import api from '@/api/index';
import { Link } from 'react-router-dom';
export default function RightRead() {
  let init = useCallback(async () => {
    const res = await api.getCategoryTag();
    setlist(res.data.data);
  }, []);
  let [list, setlist] = useState([]);
  useEffect(() => {
    init();
  }, [init]);
  return (
    <div className="articleclassify">
      <h3>文章分类</h3>
      <div className="articleclassify_list">
        <ul>
          {list && list.length > 0 ? (
            list.map((item, index) => {
              return (
                <Link key={index} to={`/category/${item.value}`}>
                  <li>
                    <span>{item.label}</span>
                    <span>共计{item.articleCount}篇文章</span>
                  </li>
                </Link>
              );
            })
          ) : (
            <div>暂无数据</div>
          )}
        </ul>
      </div>
    </div>
  );
}
