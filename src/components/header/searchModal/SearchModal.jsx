import React, { useState } from 'react';
import { Modal, Button } from 'antd';
const SearchModal = (props) => {
  const showModal = () => {
    props.setSearchModalVisible(true);
  };

  const handleOk = () => {
    props.setSearchModalVisible(false);
  };

  const handleCancel = () => {
    props.setSearchModalVisible(false);
  };
  return (
    <div>
      <Button type="primary" onClick={showModal}>
        Open Modal
      </Button>
      <Modal
        title="Basic Modal"
        visible={props.SearchModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </div>
  );
};
export default SearchModal;
