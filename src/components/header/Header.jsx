import React, { useEffect, useState, useCallback } from 'react';
import './Header.css';
import { NavLink, withRouter } from 'react-router-dom';
// 主题切换
import { dark, light } from '../../config/theme.config';
// antd 组件库
import { Modal, Input, Space, Button, Menu, Dropdown, Avatar } from 'antd';
// 获取接口数据
import api from '@/api/index.js';
import { searchArticle } from '@/api/headerSearch.js';
// 组件
import LoginModal from './loginModal/loginModal';
import { homeRouters } from '../../router/router.config';
import { inject, observer } from 'mobx-react';
import { FormattedMessage } from 'react-intl';
import Lodash from 'lodash';
const Header = observer((props) => {
  const [flag, setFlag] = useState(new Date().getHours() > 6 && new Date().getHours() <= 19);
  const [switchFlag, setSwitchFlag] = useState(false);
  // 搜索弹框是否显示
  const [SearchModalVisible, setSearchModalVisible] = useState(false);
  // 登录弹框
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  // 列表数据
  const [list, setList] = useState([]);
  // 搜索到的数据
  const [searchList, setSearchList] = useState([]);
  // 登录的用户名
  const [loginName, setLoginName] = useState('');
  const init = useCallback(async () => {
    const res = await api.getArchiveList();
    setList(res.data.data[2022].May);
    window.less.modifyVars(flag ? light : dark);
  }, [flag]);
  setTimeout(() => {
    const names = localStorage.getItem('isLogin')
      ? JSON.parse(localStorage.getItem('isLogin')).username
      : '';
    setLoginName(names);
  }, 0);
  useEffect(() => {
    init();
  }, [init]);
  // 主题切换
  const themeSwitch = () => {
    setFlag(!flag);
  };
  // 点击显示搜索弹框
  const showSearchModal = () => {
    setSearchModalVisible(true);
  };
  // 点击关闭搜索弹框
  const handleCancel = () => {
    setSearchModalVisible(false);
    setTimeout(() => {
      let searchValue = document.querySelector('.ant-input');
      searchValue.value = '';
    }, 1000);
    setSearchList([]);
  };
  // 搜索
  const { Search } = Input;
  const onSearch = Lodash.debounce(async (value) => {
    if (value) {
      const res = await searchArticle(value);
      setSearchList(res.data.data);
    } else {
      setSearchList([]);
    }
  }, 200);
  // 点击登录登录弹框显示
  const loginShowModal = useCallback(() => {
    setLoginModalVisible(true);
  }, []);
  // 点击切换英文
  const handleClickLangeUS = () => {
    props.i18nStore.changeLangeUS();
  };
  // 点击切换中文
  const handleClickLangeCN = () => {
    props.i18nStore.changeLangeCN();
  };
  // 中英文切换下拉菜单
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <div onClick={handleClickLangeUS}>
              <FormattedMessage id="english" />
            </div>
          ),
        },
        {
          key: '2',
          label: (
            <div onClick={handleClickLangeCN}>
              <FormattedMessage id="chinese" />
            </div>
          ),
        },
      ]}
    />
  );
  // 点击退出登录
  const handleLogOut = () => {
    localStorage.removeItem('isLogin');
    setLoginName('');
  };
  // 登录信息下拉菜单
  const loginMenu = (
    <Menu
      items={[
        {
          key: '1',
          label: <div>{loginName}</div>,
        },
        {
          key: '2',
          label: <div onClick={handleLogOut}>退出</div>,
        },
      ]}
    />
  );
  const goDetails = (id) => {
    props.history.push({
      pathname: '/detall',
      state: id,
    });
    setSearchModalVisible(false);
  };
  //定义下拉状态
  const [isPull, setPull] = useState(false);
  return (
    <div className="header_wrap">
      <div className="header-container">
        {/* logo */}
        <div className="header-logo">
          <a href="/home">
            <img
              src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
              alt="logo"
            />
          </a>
        </div>
        <div className="header-right">
          {/* 二级路由切换 */}
          <div className="header-nav">
            {homeRouters &&
              homeRouters.map((item, index) => {
                return (
                  <span key={index}>
                    <NavLink to={item.path} className="textcolor">
                      <FormattedMessage id={item.meta.title} />
                    </NavLink>
                  </span>
                );
              })}
          </div>
          {/* 头部右侧功能点 */}
          <div className="header-style">
            <Space direction="vertical">
              <Dropdown overlay={menu} placement="bottomLeft">
                <span className="header-style-span1">
                  <svg
                    viewBox="0 0 1024 1024"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                  >
                    <path
                      d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </span>
              </Dropdown>
            </Space>

            <span onClick={themeSwitch}>
              {flag ? (
                <svg className="sun" viewBox="0 0 24 24">
                  <path
                    fill="currentColor"
                    d="M3.55,18.54L4.96,19.95L6.76,18.16L5.34,16.74M11,22.45C11.32,22.45 13,22.45 13,22.45V19.5H11M12,5.5A6,6 0 0,0 6,11.5A6,6 0 0,0 12,17.5A6,6 0 0,0 18,11.5C18,8.18 15.31,5.5 12,5.5M20,12.5H23V10.5H20M17.24,18.16L19.04,19.95L20.45,18.54L18.66,16.74M20.45,4.46L19.04,3.05L17.24,4.84L18.66,6.26M13,0.55H11V3.5H13M4,10.5H1V12.5H4M6.76,4.84L4.96,3.05L3.55,4.46L5.34,6.26L6.76,4.84Z"
                  ></path>
                </svg>
              ) : (
                <svg className="moon" viewBox="0 0 24 24">
                  <path
                    fill="currentColor"
                    d="M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z"
                  ></path>
                </svg>
              )}
            </span>
            <span>
              <svg
                viewBox="64 64 896 896"
                focusable="false"
                data-icon="search"
                width="1em"
                height="1em"
                fill="currentColor"
                aria-hidden="true"
                onClick={showSearchModal}
              >
                <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
              </svg>
            </span>
            {loginName ? (
              <Space direction="vertical">
                <Dropdown overlay={loginMenu} placement="bottomLeft">
                  <Avatar>{loginName.charAt(0)}</Avatar>
                </Dropdown>
              </Space>
            ) : (
              <Button className="login-btn" onClick={loginShowModal}>
                登录
              </Button>
            )}
          </div>
        </div>
        <div className={isPull ? 'header_right_2 show' : 'header_right_2 hide'}>
          {homeRouters &&
            homeRouters.map((item, index) => {
              return (
                <span key={index}>
                  <NavLink to={item.path} className="textcolor">
                    <FormattedMessage id={item.meta.title} />
                  </NavLink>
                </span>
              );
            })}
          <Space direction="vertical">
            <Dropdown overlay={menu} placement="bottomLeft">
              <span className="header-style-span1">
                <svg
                  viewBox="0 0 1024 1024"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  width="1em"
                  height="1em"
                >
                  <path
                    d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
                    fill="currentColor"
                  ></path>
                </svg>
              </span>
            </Dropdown>
          </Space>
          <span onClick={themeSwitch}>
            {flag ? (
              <svg className="sun" viewBox="0 0 24 24">
                <path
                  fill="currentColor"
                  d="M3.55,18.54L4.96,19.95L6.76,18.16L5.34,16.74M11,22.45C11.32,22.45 13,22.45 13,22.45V19.5H11M12,5.5A6,6 0 0,0 6,11.5A6,6 0 0,0 12,17.5A6,6 0 0,0 18,11.5C18,8.18 15.31,5.5 12,5.5M20,12.5H23V10.5H20M17.24,18.16L19.04,19.95L20.45,18.54L18.66,16.74M20.45,4.46L19.04,3.05L17.24,4.84L18.66,6.26M13,0.55H11V3.5H13M4,10.5H1V12.5H4M6.76,4.84L4.96,3.05L3.55,4.46L5.34,6.26L6.76,4.84Z"
                ></path>
              </svg>
            ) : (
              <svg className="moon" viewBox="0 0 24 24">
                <path
                  fill="currentColor"
                  d="M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z"
                ></path>
              </svg>
            )}
          </span>
          <span>
            <svg
              viewBox="64 64 896 896"
              focusable="false"
              data-icon="search"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
              onClick={showSearchModal}
            >
              <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
            </svg>
          </span>
          {localStorage.getItem('isLogin') ? (
            <Space direction="vertical">
              <Dropdown overlay={loginMenu} placement="bottomLeft">
                <Avatar style={{ fontSize: '17px' }}>
                  {localStorage.getItem('isLogin')
                    ? JSON.parse(localStorage.getItem('isLogin')).username.charAt(0)
                    : ''}
                </Avatar>
              </Dropdown>
            </Space>
          ) : (
            <Button className="login-btn" onClick={loginShowModal}>
              登录
            </Button>
          )}
        </div>
        <div
          className={isPull ? '_1m0pVz8iNOboGDqVTeBfmy close' : '_1m0pVz8iNOboGDqVTeBfmy'}
          onClick={() => {
            setPull(!isPull);
          }}
        >
          <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
          <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
          <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
        </div>
      </div>
      {/* 搜索弹框 */}
      <div className="header-search">
        <Modal
          title="文章搜索"
          visible={SearchModalVisible}
          onCancel={handleCancel}
          keyboard={true}
          footer={null}
          style={{ width: '768px' }}
          width="768px"
        >
          <span className="escBox">esc</span>
          <Space direction="vertical">
            <Search
              placeholder="输入关键字搜索文章"
              onSearch={onSearch}
              style={{ width: 725, height: '40px' }}
              status="error"
            />
            {searchList.length > 0 ? (
              <div className="section">
                <ul>
                  {searchList.length &&
                    searchList.map((item, index) => {
                      return (
                        <li key={item.id}>
                          <div
                            onClick={() => {
                              goDetails(item.id);
                            }}
                          >
                            {item.title}
                          </div>
                        </li>
                      );
                    })}
                </ul>
              </div>
            ) : (
              <p className="empty">暂无数据</p>
            )}
          </Space>
        </Modal>
      </div>
      {/* 登录弹框 */}
      <LoginModal
        loginModalVisible={loginModalVisible}
        setLoginModalVisible={setLoginModalVisible}
      />
    </div>
  );
});
export default inject((store) => store)(withRouter(Header));
