import React, { useEffect, useState, useCallback } from 'react';
import { Modal, Form, Input, Button } from 'antd';
import './loginModal.css';
// import api from '@/api/index.js';
import { toGitee, getCode } from '@/utils/gitee.js';
const LoginModal = (props) => {
  const [code, setCode] = useState('');
  const handleCancel = () => {
    props.setLoginModalVisible(false);
  };
  const onFinish = async (values) => {
    localStorage.setItem('isLogin', JSON.stringify(values));
    props.setLoginModalVisible(false);
  };
  // 跳转第三方
  const goGithub = () => {
    toGitee();
  };
  const init = useCallback(() => {
    setCode(getCode());
    // console.log(code);
    // let res = await api.getApiGithub({code});
    // console.log(res);
  }, []);
  useEffect(() => {
    init();
  }, [init]);
  // setTimeout(()=>{
  //   console.log({code});
  //   let res =  api.getApiGithub({code}).then(res=>{
  //     console.log(res);
  //   })
  // },0)

  return (
    <div className="loginModal_wrap">
      <Modal
        title="账密登录"
        visible={props.loginModalVisible}
        onCancel={handleCancel}
        footer={null}
        width="370px"
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item name="username" rules={[{ required: true, message: '请输入您的称呼' }]}>
            <Input placeholder="名称" style={{ width: '320px', height: '30px' }} />
          </Form.Item>

          <Form.Item name="password" rules={[{ required: true, message: '请输入密码' }]}>
            <Input.Password placeholder="密码" style={{ width: '320px', height: '30px' }} />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ width: '320px', background: 'red' }}>
              登录
            </Button>
          </Form.Item>
          <Form.Item>
            <svg
              style={{ width: '20px', height: '20px', marginLeft: '10px' }}
              viewBox="64 64 896 896"
              focusable="false"
              data-icon="github"
              fill="currentColor"
              aria-hidden="true"
              className="third"
              onClick={goGithub}
            >
              <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path>
            </svg>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
export default LoginModal;
