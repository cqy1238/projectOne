import React, { useState, useCallback, useEffect } from 'react';
import { getLabelList } from '../../api/articlelabel';
import './ArticleLabel.css';
import { Link } from 'react-router-dom';
export default function RightRead() {
  let init = useCallback(async () => {
    const res = await getLabelList();
    setlist(res.data.data);
  }, []);
  let [list, setlist] = useState([]);
  useEffect(() => {
    init();
  }, [init]);
  return (
    <div className="articlelabel">
      <h3>文章标签</h3>
      <div className="articlelabel_list">
        <ul>
          {list && list.length > 0 ? (
            list.map((item, index) => {
              return (
                <Link key={index} to={`/tag/${item.value}`}>
                  <li>
                    <span>
                      {item.label} [{item.articleCount}]
                    </span>
                  </li>
                </Link>
              );
            })
          ) : (
            <div>暂无数据</div>
          )}
        </ul>
      </div>
    </div>
  );
}
