import { BackTop, Space } from 'antd';
import { VerticalAlignTopOutlined } from '@ant-design/icons';
const style = {
  height: 40,
  width: 40,
  lineHeight: '40px',
  borderRadius: '50%',
  backgroundColor: '#000',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
  position: 'fixed',
  bottom: '60px',
};

const App = () => (
  <div
    style={{
      height: '80vh',
      padding: 8,
    }}
  >
    <BackTop>
      <div style={style}>
        <Space style={{ fontSize: '24px' }}>
          <VerticalAlignTopOutlined />
        </Space>
      </div>
    </BackTop>
  </div>
);

export default App;
