import axios from '../utils/axios';
export const getComment = ({ page, pageSize }) =>
  axios.get(`/api/comment`, {
    params: {
      page: page,
      pageSize: pageSize,
    },
  });
