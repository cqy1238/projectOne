import axios from '@/utils/axios.js';
export const searchArticle = (params) =>
  axios.get('/api/search/article', {
    params: {
      keyword: params,
    },
  });
