//api入口文件
import httpTool from '../utils/httpTool';
import { message } from 'antd';
const httpAxios = httpTool({
  timeout: 5,
  commonHeaders: {
    token: localStorage.getItem('token') || '1909',
  },
  failMessage: (msg) => {
    message.warn(msg);
  },
});
//批量导入
const context = require.context('./modules', false, /\.js$/);
const dataObj = context.keys().reduce((pre, cur) => {
  return { ...pre, ...context(cur).default };
}, {});

const api = Object.keys(dataObj).reduce((pre, cur) => {
  //  port动态接口   参数
  pre[cur] = (data) => {
    return httpAxios({
      ...dataObj[cur],
      [dataObj[cur].method === 'get' ? 'params' : 'data']: data,
    });
  };
  return pre;
}, {});
export default api;
