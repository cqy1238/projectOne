import httpTool from '@/utils/httpTool';
import { message } from 'antd';
const httpAxios = httpTool({
  timeout: 2,
  commonHeaders: {
    token: localStorage.getItem('token') || '1909',
  },
  failMessage: (msg) => {
    message.warn(msg);
  },
});
export const getCategory = (params) => httpAxios.get(`/api/article/category/${params}`);
export const getTagArticle = (params) => httpAxios.get(`/api/article/tag/${params}`);
