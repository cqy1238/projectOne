import axios from '../utils/axios';
//知识小册数据请求
export const getKnowledge = () => axios.get('/api/Knowledge');
export const getKnowledgedet = (id) => axios.get(`/api/Knowledge/${id}`);
