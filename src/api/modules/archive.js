const archiveApi = {
  //获取归档页面
  getArchiveList: {
    method: 'get',
    url: '/api/article/archives',
  },
};
export default archiveApi;
