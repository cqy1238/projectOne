const headerApi = {
  getApiLogin: {
    method: 'post',
    url: '/api/auth/login',
  },
  getApiAdmin: {
    method: 'post',
    url: '/api/auth/admin',
  },
  getApiGithub: {
    method: 'post',
    url: '/api/auth/github',
  },
};
export default headerApi;
// export const searchArticle = (params) =>
//   axios.get('/api/search/article', { params: { keyword: params } });
// export const ApiLogin = (data) => axios.post('/api/auth/login', data);
// export const ApiAdmin = (data) => axios.post('/api/auth/admin', data);
