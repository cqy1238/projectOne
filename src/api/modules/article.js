const articleApi = {
  //获取文章分类标签
  getCategoryTag: {
    method: 'get',
    url: '/api/category',
  },
  //获取所有文章
  getArticle: {
    method: 'get',
    url: '/api/article',
  },
  //获取文章标签
  getArticleTag: {
    method: 'get',
    url: '/api/tag',
  },
  //获取评论数据
  getComment: {
    method: 'get',
    url: '/api/comment',
  },
  //推荐阅读
  getRecRead: {
    method: 'get',
    url: '/api/article/recommend',
  },
  //分页接口
  getPageData: {
    method: 'get',
    url: '/api/page',
  },
};
export default articleApi;
