import React, { Component } from 'react';
import './error.css';
import { Button, Result } from 'antd';

export class Error extends Component {
  state = {
    time: 5,
  };
  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({
        time: this.state.time - 1,
      });
      console.log(this.state.time);
      if (this.state.time <= 0) {
        this.props.history.push('/home/article');
        clearInterval(this.timer);
      } else {
        return null;
      }
    }, 1000);
  }
  componentWillUnmount() {
    this.timer = null;
  }
  render() {
    return (
      <div className="error">
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
          extra={
            <Button
              type="primary"
              onClick={() => {
                this.props.history.push('/home/article');
              }}
            >
              回到首页( {this.state.time}秒之后自动跳转首页)
            </Button>
          }
        />
      </div>
    );
  }
}

export default Error;
