import { BackTop } from 'antd';
import React, { Component } from 'react';
import BottomRead from '../../components/BottomRecRead/BottomRead';
import Comments from '../../components/Comments/Comments';
import './board.css';

export class board extends Component {
  componentDidMount() {
    document.title = '留言板 | 小楼又清风';
  }
  componentWillUnmount() {
    document.title = '小楼又清风';
  }
  render() {
    return (
      <div className="board">
        <div className="top">
          <p>留言板</p>
          <p>⌈ 请勿灌水 🤖⌋</p>
          <p>⌈ 垃圾评论过多，欢迎提供好的过滤（标记）算法 ⌋</p>
        </div>
        <div className="bottom">
          <BackTop />
          <Comments />
          <BottomRead />
        </div>
      </div>
    );
  }
}

export default board;
