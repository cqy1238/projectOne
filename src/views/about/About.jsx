import React, { Component } from 'react';
import { BackTop, Image } from 'antd';
import './about.css';
import BottomRead from '../../components/BottomRecRead/BottomRead';
import Comment from '../../components/Comments/Comments';

export class About extends Component {
  componentDidMount() {
    document.title = '关于 | 小楼又清风';
  }
  componentWillUnmount() {
    document.title = '小楼又清风';
  }
  state = {
    visible: false,
  };
  setVisible() {
    this.setState({
      visible: true,
    });
  }
  render() {
    let { visible } = this.state;
    return (
      <div id="about">
        <div className="top">
          <Image
            preview={false}
            // width={200}
            src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
            onClick={this.setVisible.bind(this)}
          />
          <div
            style={{
              display: 'none',
            }}
          >
            <Image.PreviewGroup
              preview={{
                visible,
                onVisibleChange: () => {
                  this.setState({
                    visible: false,
                  });
                },
              }}
            >
              <Image src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" />
              <Image src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" />
            </Image.PreviewGroup>
          </div>
          <p id="about_title">这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。</p>
          <ul className="about_ul">
            <li>更新</li>
            <li>新增 Github OAuth 登录，由于网络原因，容易失败，可以多尝试几下。</li>
            <li>
              <input type="checkbox" checked disabled /> 1
            </li>
            <li>
              <input type="checkbox" disabled /> 2
            </li>
          </ul>
        </div>
        <div className="bottom">
          <Comment />
          <BottomRead />
        </div>
        <BackTop />
      </div>
    );
  }
}

export default About;
