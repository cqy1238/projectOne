import RouterView from '../router/RouterView';
function Home(props) {
  return (
    <div className="Home">
      <RouterView routes={props.routes}></RouterView>
    </div>
  );
}

export default Home;
