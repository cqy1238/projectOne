import React, { useEffect, useCallback, useState, useRef } from 'react';
import api from '@/api/index.js';
import { Carousel, Image } from 'antd';
import { Link } from 'react-router-dom';
import ArtItem from '@/components/Item/item';
import RightRead from '@/components/RightRead/RightRead';
import ArticleLabel from '@/components/ArticleLabel/ArticleLabel';
import Share from '@/components/Share';
import BackTop from '@/components/BackTop/BackTop';
import Footer from '@/components/Footer/Footer';
import { formatTime } from '@/utils/formatTime';
import { debounce } from 'lodash';
import './article.css';
const contentStyle = {
  height: '95%',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};
const Article = ({ location, history }) => {
  //文章分类标签
  const [categoryTags, setCategoryTag] = useState([]);
  //文章数据
  const [articles, setArt] = useState([]);
  //分页接口防抖
  const [page, setPage] = useState(1);
  const load = useRef(null);

  const pageFunc = useCallback(function () {
    if (window.innerHeight - window.scrollY <= 20) {
      // load.current.style.display = 'block';
      debounce(() => {
        setPage(page + 1);
      }, 500)();
      // setTimeout(() => {
      //   // load.current.style.display = 'none';
      // }, 500);
    }
  }, []);
  const init = useCallback(async () => {
    //获取文章分类标签数据
    const categoryTagData = await api.getCategoryTag();
    setCategoryTag(categoryTagData.data.data);
    //获取文章数据
    const artData = await api.getArticle({ page, pageSize: 5 });
    setArt(articles.concat(artData.data.data[0]));
  }, [page]);
  useEffect(() => {
    init();
    //监听滚动
    window.addEventListener('scroll', pageFunc, false);
    return () => {
      window.removeEventListener('scroll', pageFunc, false);
    };
  }, [init]);

  //显示弹框
  const [vis, setVis] = useState(false);
  //设置弹窗的内容
  const [item, setItem] = useState({});
  //弹框
  const showModel = (item) => {
    setVis(true);
    setItem(item);
  };
  const hideModel = () => {
    setVis(false);
  };
  //跳转详情
  const toDetail = useCallback((id) => {
    history.push({
      pathname: '/bannerdetail',
      state: id,
    });
  }, []);
  return (
    <div className="art">
      <Share vis={vis} hideModel={hideModel} item={item} />
      <main>
        <section>
          <div className="swiper">
            <Carousel autoplay dots={'false'}>
              {articles &&
                articles.map((item) => {
                  return (
                    //首焦推荐
                    item.isRecommended && (
                      <div
                        key={item.id}
                        className={'posit'}
                        onClick={() => {
                          toDetail(item.id);
                        }}
                      >
                        <Image
                          width={'100%'}
                          height={300}
                          style={contentStyle}
                          preview={false}
                          src={
                            item.cover
                              ? item.cover
                              : 'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-12-11/IMG_031434567.jpeg'
                          }
                        />
                        <div className="text">
                          <p>{item.title}</p>
                          <p>
                            {formatTime(item.publishAt) + '前 · '}
                            {item.views}次阅读
                          </p>
                        </div>
                        <div className="mask"></div>
                      </div>
                    )
                  );
                })}
            </Carousel>
          </div>
          <div className="data">
            <div className="tag">
              <Link to={'/'} className={location.pathname === '/home/article' ? 'actived' : ''}>
                <span>所有</span>
              </Link>
              {categoryTags &&
                categoryTags.map((item) => {
                  return (
                    <Link key={item.id} to={`/category/${item.value}`}>
                      <span>{item.label}</span>
                    </Link>
                  );
                })}
            </div>
            <div className="article">
              {articles &&
                articles.map((item) => {
                  return (
                    <Link
                      to={{
                        pathname: '/detall',
                        state: item.id,
                      }}
                      key={item.id}
                    >
                      <ArtItem item={item} showModel={showModel} />
                    </Link>
                  );
                })}
              <div ref={load} className="loading">
                文章加载中......
              </div>
            </div>
          </div>
        </section>
        <aside className="article_aside">
          <RightRead />
          <ArticleLabel />
          <Footer />
          <BackTop />
        </aside>
      </main>
    </div>
  );
};

export default Article;
