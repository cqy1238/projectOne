import React, { useState, useCallback, useEffect } from 'react';
import api from '@/api/index.js';
import { getTagArticle } from '@/api/article';
import { NavLink } from 'react-router-dom';
import ArtItem from '@/components/Item/item';
import RightRead from '@/components/RightRead/RightRead';
import ArtcleClassify from '@/components/ArticleClassify/ArticleClassify';
import Share from '@/components/Share';
import BackTop from '@/components/BackTop/BackTop';
import Footer from '@/components/Footer/Footer';
import './article.css';
const Tag = ({ location, match }) => {
  //标签数据
  const [artTag, setArtTag] = useState([]);
  //   标签文章数据
  const [tagArt, setTagArt] = useState([]);
  const init = useCallback(async () => {
    //获取标签数据
    const artiTagData = await api.getArticleTag();
    setArtTag(artiTagData.data.data);
    //获取标签对应文章数据
    const tagArtData = await getTagArticle(match.params.value);
    setTagArt(tagArtData.data.data[0]);
  }, [match.params.value]);
  useEffect(() => {
    init();
  }, [init]);
  //显示弹框
  const [vis, setVis] = useState(false);
  //设置弹窗的内容
  const [item, setItem] = useState({});
  //弹框
  const showModel = (item) => {
    setVis(true);
    setItem(item);
  };
  const hideModel = () => {
    setVis(false);
  };
  return (
    <div className="art">
      <Share vis={vis} hideModel={hideModel} item={item} />
      <main>
        <section>
          <div className="head">
            <p>
              与<span>{match.params.value}</span> 标签有关的文章
            </p>
            <p>
              共搜索到 <span>{tagArt.length}</span> 篇
            </p>
          </div>
          <div className="tags">
            <div className="wzbq">文章标签</div>
            <div className="wzbq">
              {artTag &&
                artTag.map((item) => {
                  return (
                    <NavLink
                      key={item.id}
                      to={`/tag/${item.value}`}
                      className={location.pathname === `/tag/${item.value}` ? 'actived' : ''}
                    >
                      <span>
                        {item.label}[{item.articleCount}]
                      </span>
                    </NavLink>
                  );
                })}
            </div>
          </div>
          <div className="data">
            <div className="article">
              {tagArt &&
                tagArt.map((item) => {
                  return (
                    <NavLink to={`/detall/${item.id}`} key={item.id}>
                      <ArtItem item={item} showModel={showModel} />
                    </NavLink>
                  );
                })}
            </div>
          </div>
        </section>
        <aside>
          <RightRead />
          <ArtcleClassify />
          <Footer />
          <BackTop />
        </aside>
      </main>
    </div>
  );
};

export default Tag;
