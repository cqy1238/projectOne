import React, { useState, useCallback, useEffect } from 'react';
import api from '@/api/index.js';
import { getCategory } from '@/api/article';
import { NavLink } from 'react-router-dom';
import ArtItem from '@/components/Item/item';
import RightRead from '@/components/RightRead/RightRead';
import ArticleLabel from '@/components/ArticleLabel/ArticleLabel';
import Share from '@/components/Share';
import BackTop from '@/components/BackTop/BackTop';
import Footer from '@/components/Footer/Footer';
import './article.css';
const Category = ({ location, match }) => {
  //文章分类标签
  const [categoryTags, setCategoryTag] = useState([]);
  //获取分类数据
  const [category, setCategory] = useState([]);
  //标签高亮
  const init = useCallback(async () => {
    //获取文章分类标签数据
    const categoryTagData = await api.getCategoryTag();
    setCategoryTag(categoryTagData.data.data);
    //获取文章分类数据
    const categoryData = await getCategory(match.params.value);
    setCategory(categoryData.data.data[0]);
  }, [match.params.value]);

  useEffect(() => {
    init();
  }, [init]);

  //显示弹框
  const [vis, setVis] = useState(false);
  //设置弹窗的内容
  const [item, setItem] = useState({});
  //弹框
  const showModel = (item) => {
    setVis(true);
    setItem(item);
  };
  const hideModel = () => {
    setVis(false);
  };
  return (
    <div className="art">
      <Share vis={vis} hideModel={hideModel} item={item} />
      <main>
        <section>
          <div className="head">
            <p>
              <span>{match.params.value}</span> 分类文章
            </p>
            <p>
              共搜索到 <span>{category.length}</span> 篇
            </p>
          </div>
          <div className="data">
            <div className="tag">
              <NavLink to={'/'} className={location.pathname === '/home/article' ? 'actived' : ''}>
                <span>所有</span>
              </NavLink>
              {categoryTags &&
                categoryTags.map((item) => {
                  return (
                    <NavLink
                      key={item.id}
                      to={`/category/${item.value}`}
                      className={location.pathname === `/category/${item.value}` ? 'actived' : ''}
                    >
                      <span>{item.label}</span>
                    </NavLink>
                  );
                })}
            </div>
            <div className="article">
              {category &&
                category.map((item) => {
                  return (
                    <NavLink to={`/detall/${item.id}`} key={item.id}>
                      <ArtItem item={item} showModel={showModel} />
                    </NavLink>
                  );
                })}
            </div>
          </div>
        </section>
        <aside>
          <RightRead />
          <ArticleLabel />
          <Footer />
          <BackTop />
        </aside>
      </main>
    </div>
  );
};

export default Category;
