import React, { Component, createContext } from 'react';
import './detall.css';
import { Space, Badge } from 'react-vant';
import { LikeO, FontO, ShareO } from '@react-vant/icons';
import { BackTop } from 'antd';
import 'antd/dist/antd.css';
import { detallList } from '../../api/detall';
import Detail from '../../components/detallPage/detall';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import QRCode from 'qrcode.react';

import axios from '../../utils/axios';

import { Button, Modal } from 'antd';

// import { useState } from 'react';

const { confirm } = Modal;

// const config = {
//   title: 'Use Hook!',
//   content: (
//     <>
//       <ReachableContext.Consumer>{(name) => `Reachable: ${name}!`}</ReachableContext.Consumer>
//       <br />
//       <UnreachableContext.Consumer>{(name) => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
//     </>
//   ),
// };
// const [modal, contextHolder] = Modal.useModal();

class detall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
      ars: [],
      flag: false,
      content: 22,
    };
  }

  componentDidMount() {
    detallList().then((res) => {
      // console.log(res.data.data);
      this.setState({
        ars: res.data.data,
      });
    });
    // this.setState({
    //     ars:this.props.location
    // })
    console.log(this.props.location.pathname.slice(12, 100), '888888888888888');
    let str = '';
    str =
      this.props.location.state ||
      this.props.location.pathname.slice(12, 100) ||
      console.log(this.props.location);
    // axios.post(`/api/article/${str}/views`).then((res) => {
    //   // console.log(res.data.data);
    //   this.setState({
    //     arr: res.data.data,
    //   });
    // });
    axios.post(`/api/knowledge/${str}/views`).then((res) => {
      this.setState({
        arr: res.data.data,
      });
      // console.log(res.data,'999999999999999');
    });
  }
  render() {
    const { arr, flag, content, ars } = this.state;
    // console.log(arr,"***********************");
    // console.log(,'333333333333');
    console.log(ars);
    console.log(arr);
    return (
      <div className="detall">
        <div className="knowdeless">
          <Space className="demo-icon" gap={20}>
            <div className="detallicon">
              <div onClick={() => this.add()}>
                <Badge content={content}>
                  <LikeO />
                </Badge>
              </div>
              <div onClick={() => this.gun()}>
                <FontO />
              </div>
              {/* //有bug 不要放开 */}
              <div onClick={() => this.q()}>
                <span>码</span>
              </div>
            </div>
          </Space>
        </div>

        {/* 总页面 */}
        <Detail data={arr} arsList={ars}></Detail>
        {/* <div>
           <h1>推荐阅读</h1>
           {
             ars&&ars.map((v,i)=>{
               return  <div key={i}>
                   
               </div>
             })
           }

         </div> */}

        {/* 回到顶部 */}
        <div>
          <BackTop />
        </div>
      </div>
    );
  }
  //评论
  gun() {
    let comme = document.querySelector('.commect');
    comme && comme.scrollIntoView(true);
  }
  //点赞
  add() {
    // console.log(11);
    if (this.flag) {
      this.flag = false;
      //  console.log(11);
      this.setState({
        content: 22,
      });
    } else {
      this.flag = true;
      //  console.log(22);
      this.setState({
        content: 23,
      });
    }
  }
  //二维码
  q() {
    console.log(2222);
    // confirm({
    //   title: <h1>{this.state.arr.title}</h1>,
    //   icon: <ExclamationCircleOutlined />,
    //   content: (
    //     <div className="imgss">
    //       <img src={this.state.arr.cover} alt="" />
    //       <QRCode id={this.state.arr.id} value={this.state.arr.corver} />
    //     </div>
    //   ),
    //   onOk() {
    //     console.log('OK');
    //   },
    //   onCancel() {
    //     console.log('Cancel');
    //   },
  }
}

export default detall;
