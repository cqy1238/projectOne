import React, { useEffect, useState, useCallback } from 'react';
import './knowledege.modules.less';
import Share from '../../components/Share';
import { NavLink } from 'react-router-dom';
import RightRead from '../../components/RightRead/RightRead';
import Item from '../../components/Item/item';
import ArticleLabel from '../../components/ArticleClassify/ArticleClassify';
import api from '../../api/index';
import Footer from '../../components/Footer/Footer';
function KnowledgeBooklet() {
  let [data, setdata] = useState([]); // 小册数据
  let init = useCallback(async () => {
    const res = await api.getKnowledge();
    setdata(res.data.data[0]);
  }, []);
  useEffect(() => {
    init();
  }, []);

  //显示弹框
  const [vis, setVis] = useState(false);
  //设置弹窗的内容
  const [item, setItem] = useState({});
  const showModel = (item) => {
    setVis(true);
    setItem(item);
  };
  const hideModel = () => {
    setVis(false);
  };
  return (
    <div className="bigbox">
      <div className="kenleft">
        <Share vis={vis} hideModel={hideModel} item={item} />
        {data && data.length ? (
          data.map((item, index) => {
            return (
              <NavLink to={{ pathname: '/home/Knowledgedetailed', state: { item } }} key={index}>
                <Item item={item} showModel={showModel} falg={'false'}></Item>
              </NavLink>
            );
          })
        ) : (
          <div>暂无数据</div>
        )}
      </div>
      <div className="keowright">
        <RightRead></RightRead>
        <ArticleLabel></ArticleLabel>
        <Footer></Footer>
      </div>
    </div>
  );
}
export default KnowledgeBooklet;
