import React, { useEffect, useState, useCallback } from 'react';
import './knowledege.modules.less';
import { getKnowledge, getKnowledgedet } from '../../api/knowledge';
import Item from '../../components/Item/item';
import { NavLink } from 'react-router-dom';
export default function Knowledgedetailed(props) {
  let [data, setdata] = useState([]); //请求回来的数据
  let [list, setlist] = useState([]); // 渲染右侧用的数据
  const [child, setchild] = useState([]);
  let [item, setitem] = useState(props.location.state.item); //左侧数据
  const [navarr, chengenavarr] = useState(['知识小册']); // 面包屑数据
  let weint = useCallback(async () => {
    let wz = await getKnowledgedet(item.id);
    setchild(wz.data.data.children);
  });
  let init = useCallback(async () => {
    const res = await getKnowledge();
    let wz = await getKnowledgedet(item.id);
    setchild(wz.data.data.children);
    setdata(res.data.data[0]);
    let arr = JSON.parse(JSON.stringify(res.data.data[0]));
    let num = arr.findIndex((item1) => {
      return item1.id === item.id;
    });
    let arto = arr.splice(num, 1);
    setitem(arto[0]);
    setlist(arr);
  }, []);
  useEffect(() => {
    init();
  }, []);
  useEffect(() => {
    weint();
  }, [list]);

  useEffect(() => {
    let data = JSON.parse(JSON.stringify(navarr));
    let { item } = props.location.state;
    data.push(item.title);
    chengenavarr(data);
  }, []);
  const changeitem = (item) => {
    let arr = JSON.parse(JSON.stringify(data));
    let num = arr.findIndex((item1) => {
      return item1.id === item.id;
    });
    let arto = arr.splice(num, 1);
    let mbx = JSON.parse(JSON.stringify(navarr));
    mbx[1] = arto[0].title;
    chengenavarr(mbx);
    setitem(arto[0]);
    setlist(arr);
  };

  return (
    <>
      <div className="ditbig">
        <div className="detnav">
          {navarr && navarr.length
            ? navarr.map((item, index) => {
                return (
                  <span key={index}>
                    <span> {item}</span>
                    {navarr[index + 1] ? <span>/</span> : ''}
                  </span>
                );
              })
            : null}
        </div>

        <div className="detbom">
          <div className="detaleft">
            <h3>
              <b>{item.title}</b>
            </h3>
            <div className=""></div>
            <div className="detlebom">
              <div className="detlebomimg">
                <img src={item.cover} alt="" />
              </div>
              <div className="bs">
                <b>{item.title}</b>
              </div>
              <div className="bs">{item.summary}</div>
              <div className="bs">
                <span>{item.views}</span>
                <span>次阅读</span>
                <span>{item.publishAt}</span>
              </div>
              <div className="bs">
                <button
                  onClick={() => {
                    props.history.push(`/knowdetall/${child[0].id}`);
                  }}
                >
                  开始阅读
                </button>
              </div>

              <ul className="us">
                {child &&
                  child.map((item, index) => {
                    return (
                      <NavLink to={{ pathname: '/knowdetall', state: item.id }} key={index}>
                        <li
                          onMouseOut={(e) => {
                            if (e.target.tagName === 'LI') {
                              e.target.className = '';
                              e.target.children[1].style.opacity = 0;
                            }
                          }}
                          onMouseOver={(e) => {
                            if (e.target.tagName === 'LI') {
                              e.target.className = 'lis';
                              e.target.children[1].style.opacity = 1;
                            }
                          }}
                        >
                          <span className="sps">{item.title}</span>
                          <span style={{ opacity: 0 }}>{item.createAt}</span>
                        </li>
                      </NavLink>
                    );
                  })}
              </ul>
            </div>
          </div>
          <div className="rdete">
            <div className="detaright">
              <h3>
                <b>其他知识笔记</b>
              </h3>
              <div className="detribom">
                {list && list.length ? (
                  list.map((item, index) => {
                    return (
                      <div key={index} onClick={() => changeitem(item)}>
                        <Item item={item} falg={'false'}></Item>
                      </div>
                    );
                  })
                ) : (
                  <div>暂无数据</div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mgs" style={{ backgroundImage: `url(${item.cover})` }}></div>
    </>
  );
}
