import React, { Component, createContext } from 'react';
import './bannerdetail.css';
import { Space, Badge } from 'react-vant';
import { LikeO, FontO, ShareO } from '@react-vant/icons';
import { BackTop } from 'antd';
import 'antd/dist/antd.css';
// import { detallList } from '../../api/detall';
import Detail from '../../components/bannerdetail/bannerdetail';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import axios from '../../utils/axios';
import QRCode from 'qrcode.react';
// import {  Modal } from 'antd';
// const { confirm } = Modal;

import { Button, Modal } from 'antd';

// import { useState } from 'react';

const { confirm } = Modal;

// const config = {
//   title: 'Use Hook!',
//   content: (
//     <>
//       <ReachableContext.Consumer>{(name) => `Reachable: ${name}!`}</ReachableContext.Consumer>
//       <br />
//       <UnreachableContext.Consumer>{(name) => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
//     </>
//   ),
// };
// const [modal, contextHolder] = Modal.useModal();

class detall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
      ars: [],
      flag: false,
      content: 22,
    };
  }

  componentDidMount() {
    // this.setState({
    //     ars:this.props.location
    // })
    // console.log(this.props.location.pathname.slice(8,100));
    let str = '';
    str = this.props.location.state;
    // console.log(str);

    axios.post(`/api/article/${str}/views`).then((res) => {
      // console.log(res.data.data);
      this.setState({
        arr: res.data.data,
      });
    });
  }
  render() {
    const { arr, flag } = this.state;
    // console.log(arr,"***********************");
    console.log(this.props.location.state);

    console.log(arr);
    return (
      <div className="detall">
        <Space className="demo-icon" gap={20}>
          <div className="detallicon">
            <div onClick={() => this.add()}>
              <Badge content={this.state.content}>
                <LikeO />
              </Badge>
            </div>
            <div onClick={() => this.qrcode()}>
              <Space wrap>
                <ShareO />
              </Space>
            </div>
          </div>
        </Space>
        {/* 总页面 */}
        <Detail data={arr}></Detail>
        {/* 回到顶部 */}
        <div>
          <BackTop />
        </div>
      </div>
    );
  }
  //点赞
  add() {
    // console.log(11);
    if (this.flag) {
      this.flag = false;
      //  console.log(11);
      this.setState({
        content: 22,
      });
    } else {
      this.flag = true;
      //  console.log(22);
      this.setState({
        content: 23,
      });
    }
  }
  //二维码
  qrcode = () => {
    // console.log(11);
    confirm({
      title: <h1>{this.state.arr.title}</h1>,
      icon: <ExclamationCircleOutlined />,
      content: (
        <div className="imgss">
          <img src={this.state.arr.cover} alt="" />
          <QRCode id={this.state.arr.id} value={this.state.arr.corver} />
        </div>
      ),

      onOk() {
        console.log('OK');
      },

      onCancel() {
        console.log('Cancel');
      },
    });
  };
}

export default detall;
