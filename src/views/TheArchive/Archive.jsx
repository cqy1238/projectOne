import React, { useEffect, useCallback, useState, useRef } from 'react';
import './Archive.css';
import './style.css';
import RightRead from '../../components/RightRead/RightRead';
import ArticleClassify from '../../components/ArticleClassify/ArticleClassify';
import { Link } from 'react-router-dom';
import BackTop from '@/components/BackTop/BackTop';
import api from '@/api/index';
import { Sticky } from 'react-vant';
import Footer from '../../../src/components/Footer/Footer';
export default function TheArchive() {
  const [list, setlist] = useState([]);
  const [year, setyear] = useState('');
  const [month, setmonth] = useState('');
  let myRef = useRef(null);
  console.log();
  const init = useCallback(async () => {
    const res = await api.getArchiveList();
    const years = Object.keys(res.data.data)[0];
    setyear(years);

    const month = Object.keys(res.data.data[years])[0];
    setmonth(month);

    const listData = res.data.data[years][month];
    setlist(listData);
  }, []);
  useEffect(() => {
    init();
  }, [init]);
  //归档页面动画
  useEffect(() => {
    let lis = [...myRef.current.children];
    let i = 0;
    let timer = setInterval(() => {
      lis[i] && lis[i].classList.add('animate__fadeInLeft', 'show');
      i++;
      if (i >= lis.length) {
        clearInterval(timer);
      }
    }, 100);
    return () => {
      clearInterval(timer);
    };
  });

  return (
    <div className="thearchive">
      <div className="thearchive_left">
        <div className="title_box">
          <p>
            <span>归档</span>
          </p>
          <p>
            共计 <span>{list.length}</span> 篇
          </p>
        </div>
        <div className="year_box">
          <p>{year}</p>
          <p>{month}</p>
        </div>
        <div className="list_box">
          <ul ref={myRef}>
            {list && list.length > 0 ? (
              list.map((item, index) => {
                return (
                  <li key={index}>
                    <Link to={`/detall/${item.id}`}>
                      <span>{item.publishAt.slice(5, 10)}</span>
                      &emsp;
                      <span>{item.title}</span>
                    </Link>
                  </li>
                );
              })
            ) : (
              <div>暂无数据</div>
            )}
          </ul>
        </div>
      </div>

      <div className="thearchive_right">
        <Sticky offsetTop={15}>
          <div className="thearchive_top">
            <RightRead></RightRead>
          </div>
          <div className="thearchive_bottom">
            <ArticleClassify></ArticleClassify>
          </div>
          <div>
            <Footer></Footer>
          </div>
        </Sticky>
        <BackTop></BackTop>
      </div>
    </div>
  );
}
