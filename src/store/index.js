const context = require.context('./modules', false, /\.js$/);

const allMobx = context.keys().reduce((pre, cur) => {
  let key = cur.match(/^\.\/(\w+)\.js$/)[1];
  pre[key] = context(cur).default;
  return pre;
}, {});
export default allMobx;
