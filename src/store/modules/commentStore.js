import { makeAutoObservable } from 'mobx';
import { getComment } from '../../api/comment';

class commentStore {
  list = [];
  length = 0;
  pageNum = 1;
  pageSize = 6;
  constructor() {
    makeAutoObservable(this);
  }
  setPageNum(pageNum) {
    this.pageNum = pageNum;
    this.getList();
  }
  // 获取评论数据，接口动态传参
  getList() {
    getComment({ page: this.pageNum, pageSize: this.pageNum * this.pageSize }).then((res) => {
      this.list = res.data.data[0].slice(
        (this.pageNum - 1) * this.pageSize,
        this.pageNum * this.pageSize
      );
      this.length = res.data.data[1];
    });
  }
}

export default new commentStore();
