import { makeAutoObservable } from 'mobx';
import zhCN from '@/i18n/zh_CN.js';
import enUS from '@/i18n/en_US.js';

const messageLange = {
  'zh-CN': zhCN,
  'en-US': enUS,
};
class i18n {
  lange = navigator.language;
  message = messageLange[this.lange];
  constructor() {
    //变成响应式类
    makeAutoObservable(this);
  }

  changeLange() {
    // 更改语言和语言配置
    this.lange = this.lange == 'zh-CN' ? 'en-US' : 'zh-CN';
    this.message = messageLange[this.lange];
    console.log(this.lange, this.message);
  }
  changeLangeUS() {
    this.lange = 'en-US';
    this.message = messageLange[this.lange];
  }
  changeLangeCN() {
    this.lange = 'zh-CN';
    this.message = messageLange[this.lange];
  }
}

export default new i18n();
