import { Component, useEffect } from 'react';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';
import RouterView from './router/RouterView';
import routes from './router/router.config';
import Header from './components/header/Header.jsx';
import store from './store/index';
import { Provider } from 'mobx-react';
import { IntlProvider } from 'react-intl';
import { inject, observer } from 'mobx-react';
const App = observer((props) => {
  return (
    <Provider {...store}>
      <div className="App">
        <IntlProvider
          messages={props.store.i18nStore.message}
          locale={props.store.i18nStore.lange}
          defaultLocale="en"
        >
          <Router>
            <Header />
            <RouterView routes={routes}></RouterView>
          </Router>
        </IntlProvider>
      </div>
    </Provider>
  );
});

export default inject((store) => store)(App);
