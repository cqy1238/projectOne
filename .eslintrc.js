module.exports = {
  extends: ['react-app', 'react-app/jest', 'plugin:prettier/recommended'],
  rules: {
    'prettier/prettier': 1,
    'no-console': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-alert': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-debugger': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-unused-vars': [
      'warn',
      {
        vars: 'all',
        args: 'none',
      },
    ],
  },
};
