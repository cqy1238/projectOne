const { generateTheme } = require('antd-theme-generator');
const path = require('path');
const options = {
  antDir: path.join(__dirname, './node_modules/antd'),
  stylesDir: path.join(__dirname, './src'), // all files with .less extension will be processed
  varFile: path.join(__dirname, './src/styles/variables.less'), // default path is Ant Design default.less file
  themeVariables: [
    '@primary-color',
    '@box-color',
    '@text-color',
    '@thearchive-text-color',
    '@thearchive-time-color',
    '@thearchive-april-color',
    '@thearchive-line-color',
    '@article-list-color',
    '@border-color-base',
    '@header-bg-color',
    '@header-text-color',
    '@item-line-color',
    '@comment-btn-color',
    '@comment-border-color',
    '@page-active-color',
    '@footer-img-color',
    '@footer-text-color',
    '@emoji-logo-color',
  ],
  outputFilePath: path.join(__dirname, './public/color.less'), // if provided, file will be created with generated less/styles
};

generateTheme(options)
  .then((less) => {
    console.log('Theme generated successfully');
  })
  .catch((error) => {
    console.log('Error', error);
  });
